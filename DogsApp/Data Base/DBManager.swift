//
//  DBManager.swift
//  DogsApp
//
//  Created by Ariel Díaz on 08/11/23.
//

import Foundation
import CoreData

protocol CoreDataManagerProtocol {
    var persistentContainer: NSPersistentContainer { get set }
    var context: NSManagedObjectContext { get }
    var queue: DispatchQueue { get }
    func saveContext ()
}

class CoreDataManager: CoreDataManagerProtocol {
    static var shared: CoreDataManager = CoreDataManager()

    private init() {
        self.context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
    }

    var queue = DispatchQueue(
        label: "Dogs.CoreData.Manager.DataSource",
        qos: .userInteractive,
        attributes: .concurrent,
        autoreleaseFrequency: .inherit,
        target: .main
    )

    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: { (/*storeDescription*/_, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support
    var context: NSManagedObjectContext {
        return persistentContainer.viewContext
    }

    func saveContext () {
        if context.hasChanges {
            try? context.save()
        }
    }

}
