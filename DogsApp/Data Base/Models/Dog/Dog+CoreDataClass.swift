//
//  Dog+CoreDataClass.swift
//  DogsApp
//
//  Created by Ariel Díaz on 08/11/23.
//
//

import Foundation
import CoreData

@objc(Dog)
public class Dog: NSManagedObject {

    required public convenience init(from decoder: Decoder) throws {
        let context = CoreDataManager.shared.context
        guard let entity = NSEntityDescription.entity(forEntityName: "Dog", in: context) else {
                fatalError("Failed to decode Dog")
        }
        self.init(entity: entity, insertInto: context)

        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.desc = try container.decodeIfPresent(String.self, forKey: .description)
        self.age = try container.decodeIfPresent(Int16.self, forKey: .age) ?? 0
        self.url = try container.decodeIfPresent(String.self, forKey: .url)
    }

}

extension Dog: Decodable {

    enum CodingKeys: String, CodingKey {
        case name = "dogName"
        case description
        case age
        case url = "image"
    }

}
