//
//  Dog+CoreDataProperties.swift
//  DogsApp
//
//  Created by Ariel Díaz on 08/11/23.
//
//

import Foundation
import CoreData


extension Dog {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Dog> {
        return NSFetchRequest<Dog>(entityName: "Dog")
    }

    @NSManaged public var name: String?
    @NSManaged public var age: Int16
    @NSManaged public var url: String?
    @NSManaged public var desc: String?

}

extension Dog : Identifiable {

}
