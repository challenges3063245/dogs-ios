//
//  DataSource.swift
//  DogsApp
//
//  Created by Ariel Díaz on 08/11/23.
//

import Foundation
import CoreData
import Combine

typealias Completion = () -> Void

fileprivate let genericCoreDataError = NSError(domain: "the Operation can´t be completed", code: -1, userInfo: [:])

/// - DataSource is a GENERIC class to CRUD objects on CoreData
/// - DataSource class support all object that inherits from NSManagedObject
/// - Parameters
///     -   T: the Type of objects that we wants to CRUD
class DataSource {
    static let shared = DataSource()
    /// coreDataManager is an objects that contains all access to CoreData DB
    private let coreDataManager: CoreDataManagerProtocol

    /// this var only instance when the Data Base is empty for the current T object type
    static let emptyError: NSError = NSError(
        domain: "Can´t find any record",
        code: 0, userInfo: [:]
    )

    private init(manager: CoreDataManagerProtocol = CoreDataManager.shared) {
        self.coreDataManager = manager
    }

    /// - block returns a secure read/write threat to operate on CoreData DB
    /// - private access modifier for in house objects use only
    /// - Parameters
    ///     - completion: the closure that run the operation block on each method for DataSource class
    ///     - use barrier  flag in order to prevent data race condition
    private func block(completion: @escaping Completion) {
        self.coreDataManager.queue.async(group: nil, qos: .userInteractive, flags: .barrier, execute: completion)
    }

    /// This method allows you to fetch all objects of type T in a infered way
    func fetchObjects<T: NSManagedObject>() -> Future<[T], Error> {
        return Future() { [weak self] promise in
            self?.block { [weak self] in
                guard let objects = try? self?.coreDataManager.context.fetch(T.fetchRequest()) as? [T] else {
                    promise(.failure(genericCoreDataError))
                    return
                }
                let emptyError = DataSource.emptyError
                objects.isEmpty ? promise(.failure(emptyError)) : promise(.success(objects))
            } ?? promise(.failure(genericCoreDataError))
        }
    }

    /// This method delete all objects of T type is usually called when the user Log out
    func deleteObjects<T: NSManagedObject>(of type: T.Type) {
        block { [weak self] in
            let fetchRequest: NSFetchRequest<NSFetchRequestResult> = T.fetchRequest()
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            _ = try? self?.coreDataManager.context.execute(deleteRequest)
            try? self?.coreDataManager.context.save()
        }
    }

    /// This method save the data into the CoreData container when has changes to save
    /// Feel fre to use on multi threat calls
    func saveContext() {
        block { [weak self] in
            guard self?.coreDataManager.context.hasChanges ?? false else { return }
            _ = try? self?.coreDataManager.context.save()
        }
    }
}
