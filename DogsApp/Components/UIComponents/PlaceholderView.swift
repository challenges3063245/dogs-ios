//
//  PlaceholderView.swift
//  DogsApp
//
//  Created by Ariel Díaz on 08/11/23.
//

import UIKit
import Combine

class PlaceholderView: UIView {

    let retryActionPublisher = PassthroughSubject<Void, Never>()

    private lazy var imageView: CustomImageView = {
        let image = CustomImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()

    private lazy var label: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .lightGray
        label.numberOfLines = 0
        return label
    }()

    private lazy var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .large)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        return indicator
    }()

    private lazy var button: UIButton = { [weak self] in
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Try again", for: .normal)
        button.setTitleColor(.lightGray, for: .normal)
        button.addTarget(self, action: #selector(self?.retryAction(_:)), for: .touchUpInside)
        button.layer.cornerRadius = 10
        button.layer.borderColor = UIColor.lightGray.cgColor
        button.layer.borderWidth = 3
        return button
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setViews()
    }

    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        self.setLayoutConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setViews() {
        [imageView, label, button, activityIndicator].forEach(addSubview)
    }

    private func setLayoutConstraints() {
        NSLayoutConstraint.activate([
            imageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            imageView.centerYAnchor.constraint(equalTo: centerYAnchor),

            activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            activityIndicator.topAnchor.constraint(equalTo: imageView.bottomAnchor),

            label.topAnchor.constraint(equalTo: activityIndicator.bottomAnchor, constant: 8),
            label.centerXAnchor.constraint(equalTo: centerXAnchor),

            button.centerXAnchor.constraint(equalTo: centerXAnchor),
            button.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 40),
            button.widthAnchor.constraint(equalToConstant: 150)
        ])
    }

    @objc private func retryAction(_ sender: UIButton) {
        self.retryActionPublisher.send()
    }

    func start() {
        self.button.isHidden = true
        self.imageView.isHidden = true
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        self.label.isHidden = false
        self.label.text = "Cargando..."
    }

    func showView(with error: Error) {
        let err0r = error as NSError
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
        self.label.text = err0r.domain
        self.button.isHidden = false
        self.imageView.isHidden = false
        if err0r.code == -1009 {
            self.imageView.setNetworkErrorImage()
        }
    }
}
