//
//  CustomImageView.swift
//  DogsApp
//
//  Created by Ariel Díaz on 09/11/23.
//

import UIKit

class CustomImageView: UIImageView {

    private let activityView = CustomActivityView(style: .medium)

    func addLoading() {
        self.activityView.translatesAutoresizingMaskIntoConstraints = false
        self.activityView.startAnimating()
        self.activityView.isHidden = false
        self.addSubview(activityView)
    }

    func removeLoading() {
        self.activityView.stopAnimating()
        self.activityView.isHidden = true
        self.activityView.removeFromSuperview()
    }

}

class CustomActivityView: UIActivityIndicatorView {

    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        self.setLayoutConstraints()
    }

    private func setLayoutConstraints() {
        guard let superview else { return }
        NSLayoutConstraint.activate([
            centerXAnchor.constraint(equalTo: superview.centerXAnchor),
            centerYAnchor.constraint(equalTo: superview.centerYAnchor)
        ])
    }

}
