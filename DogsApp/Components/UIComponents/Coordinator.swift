//
//  Coordinator.swift
//  DogsApp
//
//  Created by Ariel Díaz on 08/11/23.
//

import Foundation

protocol Coordinator {
    func start()
    func coordinate(to coordinator: Coordinator)
}

extension Coordinator {
    func coordinate(to coordinator: Coordinator) {
        coordinator.start()
    }
}
