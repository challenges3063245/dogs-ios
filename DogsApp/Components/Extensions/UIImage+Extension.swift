//
//  UIImage+Extension.swift
//  DogsApp
//
//  Created by Ariel Díaz on 09/11/23.
//

import UIKit
import Combine

extension CustomImageView {

    func image(with url: String) {
        self.addLoading()
        NetworkAPI.shared.downloadImageData(with: url)
            .sink(receiveCompletion: { [weak self] completion in
                DispatchQueue.main.async { [weak self] in
                    self?.removeLoading()
                }
            }, receiveValue: { [weak self] data in
                DispatchQueue.main.async { [weak self] in
                    self?.image = UIImage(data: data)
                }
            })
            .store(in: &NetworkAPI.shared.subscriptions)
    }

    func setNetworkErrorImage() {
        var config = UIImage.SymbolConfiguration(paletteColors: [.systemYellow, .systemRed])
        config = config.applying(UIImage.SymbolConfiguration(font: .systemFont(ofSize: 42.0)))
        config = config.applying(UIImage.SymbolConfiguration(weight: .bold))
        let image = UIImage(systemName: "wifi.exclamationmark", withConfiguration: config)!
        self.image = image
    }
}
