//
//  API.swift
//  DogsApp
//
//  Created by Ariel Díaz on 08/11/23.
//

import Foundation
import Combine

class NetworkAPI {

    static let shared: NetworkAPI = NetworkAPI()
    static let error = NSError(domain: "Internet connection appears to be offline", code: -1009)

    var subscriptions = Set<AnyCancellable>()

    private init() { }

    func getData(with request: URLRequest) -> Future<Data, Error> {
        return Future() { promise in
            if Reachability.isConnectedToNetwork() {
                DispatchQueue.global(qos: .background).async {
                    let task = URLSession.shared.dataTask(with: request) { (data, response, error) -> Void in
                        if let data {
                            promise(Result.success(data))
                        } else if let error {
                            promise(Result.failure(error))
                        }
                    }
                    task.resume()
                }
            } else {
                promise(.failure(NetworkAPI.error as Error))
            }
        }
    }

    func downloadImageData(with url: String) -> Future<Data, Error> {
        return Future() { promise in
            if Reachability.isConnectedToNetwork() {
                DispatchQueue.global(qos: .background).async {
                    Cache.shared.downloadContent(fromUrlString: url) { completion in
                        promise(completion)
                    }
                }
            } else {
                promise(.failure(NetworkAPI.error as Error))
            }
        }
    }
}
