//
//  URLCache.swift
//  DogsApp
//
//  Created by Ariel Díaz on 09/11/23.
//

import Foundation

typealias DownloadCompletionHandler = (Result<Data,Error>) -> Void

class Cache {
    static let shared: Cache = Cache()
    private let allowedDiskSize = 100 * 1024 * 1024
    private lazy var cache: URLCache = {
        return URLCache(memoryCapacity: 0, diskCapacity: allowedDiskSize, diskPath: "imageCache")
    }()

    private init() { }

    private func createAndRetrieveURLSession() -> URLSession {
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.requestCachePolicy = .returnCacheDataElseLoad
        sessionConfiguration.urlCache = cache
        return URLSession(configuration: sessionConfiguration)
    }

    func downloadContent(fromUrlString: String, completionHandler: @escaping DownloadCompletionHandler) {

        guard let downloadUrl = URL(string: fromUrlString) else { return }
        let urlRequest = URLRequest(url: downloadUrl)
        if let cachedData = self.cache.cachedResponse(for: urlRequest) {
            debugPrint("[💾]: Cached image data:", cachedData.data)
            completionHandler(.success(cachedData.data))

        } else {
            debugPrint("[🌐]: fetch image data")
            let session = self.createAndRetrieveURLSession()
            session.dataTask(with: urlRequest) { (data, response, error) in
                if let error = error {
                    completionHandler(.failure(error))
                } else {
                    let cachedData = CachedURLResponse(response: response!, data: data!)
                    self.cache.storeCachedResponse(cachedData, for: urlRequest)

                    completionHandler(.success(data!))
                }
            }.resume()
        }
    }
}
