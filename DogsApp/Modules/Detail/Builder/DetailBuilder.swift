//
//  DetailBuilder.swift
//  DogsApp
//
//  Created by Ariel Díaz on 10/11/23.
//

import Foundation
import UIKit

class DetailBuilder {

    func build(dataDog: DogViewData) -> DetailViewController {
        let viewModel = DetailViewModel(dataDog: dataDog)
        let viewController = DetailViewController(viewModel: viewModel)
        return viewController
    }

}
