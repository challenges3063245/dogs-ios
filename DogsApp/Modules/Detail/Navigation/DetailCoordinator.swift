//
//  DetailCoordinator.swift
//  DogsApp
//
//  Created by Ariel Díaz on 10/11/23.
//

import Foundation
import UIKit

class DetailCoordinator: Coordinator {

    weak var navigationController: UINavigationController?
    var dataDog: DogViewData?

    init(navigationController: UINavigationController? = nil, dataDog: DogViewData) {
        self.navigationController = navigationController
        self.dataDog = dataDog
    }

    func start() {
        guard let dataDog else { return }
        let builder = DetailBuilder()
        let controller = builder.build(dataDog: dataDog)

        self.navigationController?.pushViewController(controller, animated: true)
    }

}
