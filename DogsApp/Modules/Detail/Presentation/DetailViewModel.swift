//
//  DetailViewModel.swift
//  DogsApp
//
//  Created by Ariel Díaz on 10/11/23.
//

import Foundation
import Combine

protocol DetailViewModelProtocol {
    var output: DetailViewModelOutput { get }
    func bind(input: DetailViewModelInput) -> DetailViewModelOutput
}

class DetailViewModel: DetailViewModelProtocol {

    let output: DetailViewModelOutput
    private let dataDog: DogViewData
    private var subscriptions = Set<AnyCancellable>()

    init(dataDog: DogViewData, output: DetailViewModelOutput = DetailViewModelOutput()) {
        self.dataDog = dataDog
        self.output = output
    }

    func bind(input: DetailViewModelInput) -> DetailViewModelOutput {
        input.didLoadPublisher.sink(receiveValue: didLoadHandler).store(in: &subscriptions)

        return self.output
    }

    private lazy var didLoadHandler: Completion = { [weak self] in
        guard let self else { return }
        self.output.updateUIPublisher.send(self.dataDog)
    }
}
