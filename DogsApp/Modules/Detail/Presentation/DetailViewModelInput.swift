//
//  DetailViewModelInput.swift
//  DogsApp
//
//  Created by Ariel Díaz on 11/11/23.
//

import Combine
import Foundation

struct DetailViewModelInput {
    let didLoadPublisher = PassthroughSubject<Void, Never>()
}
