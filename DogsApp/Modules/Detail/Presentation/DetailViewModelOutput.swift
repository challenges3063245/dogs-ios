//
//  DetailViewModelOutput.swift
//  DogsApp
//
//  Created by Ariel Díaz on 11/11/23.
//

import Foundation
import Combine

struct DetailViewModelOutput {
    let updateUIPublisher = PassthroughSubject<DogViewData, Never>()
}
