//
//  DetailViewController.swift
//  DogsApp
//
//  Created by Ariel Díaz on 10/11/23.
//

import UIKit
import Combine

class DetailViewController: UIViewController {

    let viewModel: DetailViewModel
    private let viewModelInput: DetailViewModelInput
    private var subscriptions = Set<AnyCancellable>()

    lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 6.0
        scrollView.delegate = self
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        return scrollView
    }()

    lazy var imageView: CustomImageView = {
        let image = CustomImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        return image
    }()

    var statusBarHeight: CGFloat {
        UIApplication.shared.connectedScenes
                .filter { $0.activationState == .foregroundActive }
                .compactMap { $0 as? UIWindowScene }
                .first?.windows
                .filter { $0.isKeyWindow }.first?
                .windowScene?.statusBarManager?.statusBarFrame.height ?? 0
    }

    init(viewModel: DetailViewModel, viewModelInput: DetailViewModelInput = DetailViewModelInput()) {
        self.viewModel = viewModel
        self.viewModelInput = viewModelInput
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        self.setupLayoutViews()
        self.setupLayoutConstraints()
        self.bind()
        self.viewModelInput.didLoadPublisher.send()
    }

    override func setNeedsUpdateOfSupportedInterfaceOrientations() {
        super.setNeedsUpdateOfSupportedInterfaceOrientations()
        self.setupLayoutConstraints()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
            self?.scrollView.setContentOffset(.zero, animated: true)
        }
    }

    private func setupLayoutViews() {
        [scrollView].forEach(view.addSubview)
        [imageView].forEach(scrollView.addSubview)
        view.insetsLayoutMarginsFromSafeArea = false
    }

    private func setupLayoutConstraints() {
        let oldConstraints: [NSLayoutConstraint] = view.constraints.filter {
            $0.firstItem?.isKind(of: UIScrollView.self) ?? false
        } + scrollView.constraints
        
        let oldImageConstraints: [NSLayoutConstraint] = view.constraints.filter {
            $0.firstItem?.isKind(of: UIImageView.self) ?? false
        } + imageView.constraints

        NSLayoutConstraint.deactivate(oldConstraints + oldImageConstraints)
        NSLayoutConstraint.activate([
            scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            scrollView.heightAnchor.constraint(equalTo: view.heightAnchor, constant: statusBarHeight),
            scrollView.widthAnchor.constraint(equalTo: view.widthAnchor),
            scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: -statusBarHeight),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),

            imageView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            imageView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            imageView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            imageView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            imageView.heightAnchor.constraint(equalTo: view.heightAnchor),
            imageView.widthAnchor.constraint(equalTo: view.widthAnchor)
        ])
    }

    func bind() {
        let output = self.viewModel.bind(input: viewModelInput)

        output.updateUIPublisher.sink(receiveValue: updateUIHandler).store(in: &subscriptions)
    }

    private lazy var updateUIHandler: (DogViewData) -> Void = { [weak self] viewData in
        guard let urlString = viewData.url else { return }
        self?.imageView.image(with: urlString)
    }
}

// MARK: - UIScrollViewDelegate
extension DetailViewController: UIScrollViewDelegate {

    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }

    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        let isZoomed: Bool = scrollView.zoomScale > scrollView.minimumZoomScale
        self.navigationController?.navigationBar.isHidden = isZoomed
        self.scrollView.showsVerticalScrollIndicator = isZoomed
        self.scrollView.showsHorizontalScrollIndicator = isZoomed
    }

}
