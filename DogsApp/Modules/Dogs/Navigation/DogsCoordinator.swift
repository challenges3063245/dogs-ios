//
//  Coordinator.swift
//  DogsApp
//
//  Created by Ariel Díaz on 08/11/23.
//

import Foundation
import UIKit
import Combine

class DogsCoordinator: Coordinator {

    private var navigationController: UINavigationController
    private var subscriptions = Set<AnyCancellable>()

    var controller: UIViewController {
        self.navigationController
    }

    init(navigationController: UINavigationController = UINavigationController()) {
        self.navigationController = navigationController
    }

    func start() {
        let builder = DogsBuilder()
        let viewController = builder.build()
        let output = viewController.viewModel.output

        output.coordinateToDetailPublisher.sink(receiveValue: coordinateToDetailHandler).store(in: &subscriptions)

        self.navigationController = UINavigationController(rootViewController: viewController)
    }

    private lazy var coordinateToDetailHandler: (DogViewData) -> Void = { [weak self] viewData in
        let coordinator = DetailCoordinator(navigationController: self?.navigationController, dataDog: viewData)
        self?.coordinate(to: coordinator)
    }
}
