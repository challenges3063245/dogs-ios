//
//  Builder.swift
//  DogsApp
//
//  Created by Ariel Díaz on 08/11/23.
//

import Foundation
import UIKit

class DogsBuilder {

    func build() -> DogsViewController {
        let repository = DogsRepository()
        let useCase = DogsUseCase(repository: repository)
        let viewModel = DogsViewModel(useCase: useCase)
        let controller = DogsViewController(viewModel: viewModel)
        return controller
    }

}
