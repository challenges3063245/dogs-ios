//
//  DogsUseCase.swift
//  DogsApp
//
//  Created by Ariel Díaz on 08/11/23.
//

import Foundation
import Combine

class DogsUseCase {

    private let repository: DogsRepository

    init(repository: DogsRepository) {
        self.repository = repository
    }

    func fetchDogs() -> AnyPublisher<[Dog], Error> {
        return self.repository.fetchDogs()
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }

    func getAvailableDogs() -> AnyPublisher<[Dog], Error> {
        return self.repository.getAvailableDogs()
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }

}
