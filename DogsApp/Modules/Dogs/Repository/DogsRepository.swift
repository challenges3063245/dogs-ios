//
//  Repository.swift
//  DogsApp
//
//  Created by Ariel Díaz on 08/11/23.
//

import Foundation
import Combine

class DogsRepository {
    private let client: NetworkAPI
    private let dbClient: DataSource
    private var subscriptions = Set<AnyCancellable>()
    private let decoder = JSONDecoder()

    init(client: NetworkAPI = NetworkAPI.shared, dbClient: DataSource = DataSource.shared) {
        self.client = client
        self.dbClient = dbClient
    }

    func fetchDogs() -> AnyPublisher<[Dog], Error> {
        let url = URL(string: "https://jsonblob.com/api/1151549092634943488")!
        let request = URLRequest(url: url)
        return self.client.getData(with: request)
            .tryMap { [weak self] data in
                guard let dogs = (try? self?.decoder.decode([Dog].self, from: data)) else {
                    throw NSError(domain: "🚧 \n\nCan not serialize data", code: 500) as Error
                }
                DataSource.shared.deleteObjects(of: Dog.self)
                DataSource.shared.saveContext()
                return dogs
            }.mapError {
                $0
            }
            .eraseToAnyPublisher()
    }

    func getAvailableDogs() -> AnyPublisher<[Dog], Error> {
        return self.dbClient.fetchObjects().eraseToAnyPublisher()
    }

}
