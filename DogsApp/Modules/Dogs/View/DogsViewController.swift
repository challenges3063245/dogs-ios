//
//  ViewController.swift
//  DogsApp
//
//  Created by Ariel Díaz on 07/11/23.
//

import UIKit
import Combine

class DogsViewController: UIViewController {

    var viewModel: DogsViewModelProtocol
    private let viewModelInput: DogsViewModelInput
    private var viewData: DogsViewData?
    private var subscriptions = Set<AnyCancellable>()

    lazy var placeholderView: PlaceholderView = {
        let view = PlaceholderView()
        return view
    }()

    lazy var refreshControl: UIRefreshControl = { [weak self] in
        let control = UIRefreshControl()
        let selector = #selector(self?.refresh(_:))
        control.addTarget(self, action: selector, for: .valueChanged)
        return control
    }()

    lazy var tableView: UITableView = { [weak self] in
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.refreshControl = self?.refreshControl
        tableView.showsVerticalScrollIndicator = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundView = self?.placeholderView
        tableView.register(DogTableViewCell.self, forCellReuseIdentifier: DogTableViewCell.id)
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        return tableView
    }()

    init(viewModel: DogsViewModelProtocol, input: DogsViewModelInput = DogsViewModelInput()) {
        self.viewModel = viewModel
        self.viewModelInput = input
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = .label
        self.setViews()
        self.setLayoutConstraints()
        self.bind()
        self.viewModelInput.didLoadPublisher.send()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }

    @objc private func refresh(_ sender: UIRefreshControl) {
//        sender.endRefreshing()
        self.viewModelInput.forceFetchDogsPublisher.send()
    }

    private func setViews() {
        self.title = "Dogs We Love"
        self.view.backgroundColor = .hexF8F8F8

        self.view.addSubview(tableView)
        self.placeholderView.start()
    }

    private func setLayoutConstraints() {
        NSLayoutConstraint.activate([
            tableView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            tableView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            tableView.heightAnchor.constraint(equalTo: view.heightAnchor),
            tableView.widthAnchor.constraint(equalTo: view.widthAnchor)
        ])
    }

    // MARK: - binding
    /// -   bind function allows the viewController to send and receive messages with the viewModel through input/output
    private func bind() {
        let output = self.viewModel.bind(input: viewModelInput)
        
        output.updateUIPublisher.sink(receiveValue: updateUIHandler).store(in: &subscriptions)

        output.updateUIErrorPublisher.sink(receiveValue: updateUIErrorHandler).store(in: &subscriptions)

        self.placeholderView.retryActionPublisher.sink(receiveValue: retryActionHandler).store(in: &subscriptions)
    }

    // MARK: - Completion handlers
    private lazy var updateUIHandler: (DogsViewData) -> Void = { [weak self] viewData in
        guard let self = self else { return }
        self.viewData = viewData
        self.placeholderView.isHidden = true
        self.tableView.reloadData()
        self.refreshControl.endRefreshing()
    }

    private lazy var updateUIErrorHandler: (Error) -> Void = { [weak self] error in
        self?.refreshControl.endRefreshing()
        self?.placeholderView.showView(with: error)
    }

    private lazy var retryActionHandler: Completion = { [weak self] in
        self?.viewModelInput.forceFetchDogsPublisher.send()
        self?.placeholderView.start()
    }
}

// MARK: - UITableViewDelegate & UITableViewDataSource
extension DogsViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewData?.numberOfRows ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DogTableViewCell.id, for: indexPath) as? DogTableViewCell,
              let viewData = self.viewData?.dogViewModels[indexPath.row] else {
            return UITableViewCell()
        }
        cell.setCell(with: viewData)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModelInput.didSelectRowPublisher.send(indexPath)
    }
}

#Preview {
    DogsCoordinator().controller
}
