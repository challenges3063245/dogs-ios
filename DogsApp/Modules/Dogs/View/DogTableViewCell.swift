//
//  DogTableViewCell.swift
//  DogsApp
//
//  Created by Ariel Díaz on 09/11/23.
//

import UIKit

class DogTableViewCell: UITableViewCell {

    static var id: String = "cell_id"

    private lazy var image: CustomImageView = {
        let imageView = CustomImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 10
        imageView.clipsToBounds = true
        return imageView
    }()

    private lazy var labelName: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textColor = .hex333333
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var labelDescription: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .hex666666
        label.font = UIFont.systemFont(ofSize: 13)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var labelAge: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textColor = .hex333333
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var labelContainer: UIView = {
        let view = UIStackView(arrangedSubviews: [labelName, labelDescription, labelAge])
        view.axis = .vertical
        view.backgroundColor = .systemBackground
        view.layoutMargins = UIEdgeInsets(top: 20, left: 40, bottom: 20, right: 8)
        view.isLayoutMarginsRelativeArrangement = true
        view.layer.cornerRadius = 10
        view.spacing = 2
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setLayoutViews()
        self.setLayoutConstraints()
        self.selectionStyle = .none
        self.backgroundColor = .clear
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setLayoutViews() {
        [labelContainer, image].forEach(addSubview)
    }

    private func setLayoutConstraints() {
        NSLayoutConstraint.activate([
            image.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 16),
            image.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            image.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
            image.heightAnchor.constraint(equalToConstant: 250),
            image.widthAnchor.constraint(equalTo: image.heightAnchor, multiplier: 0.7),

            labelContainer.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -16),
            labelContainer.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
            labelContainer.leadingAnchor.constraint(equalTo: image.trailingAnchor, constant: -16),
            labelContainer.heightAnchor.constraint(equalTo: image.heightAnchor, multiplier: 0.7)
        ])
    }

    func setCell(with viewData: DogViewData) {
        self.labelName.text = viewData.name
        self.labelDescription.text = viewData.desc
        self.labelAge.text = viewData.age
        guard let url = viewData.url else { return }
        self.image.image(with: url)
    }

}
