//
//  DogViewData.swift
//  DogsApp
//
//  Created by Ariel Díaz on 08/11/23.
//

import Foundation

struct DogViewData {
    private let dog: Dog

    var name: String? {
        dog.name
    }

    var desc: String? {
        dog.desc
    }

    var age: String? {
        "Almost \(dog.age) years"
    }

    var url: String? {
        dog.url
    }

    init(_ dog: Dog) {
        self.dog = dog
    }
}
