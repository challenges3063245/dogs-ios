//
//  DogsViewData.swift
//  DogsApp
//
//  Created by Ariel Díaz on 08/11/23.
//

import Foundation

struct DogsViewData {

    var dogViewModels: [DogViewData]

    var numberOfRows: Int {
        self.dogViewModels.count
    }

    init(_ dogViewModels: [DogViewData]) {
        self.dogViewModels = dogViewModels
    }
}
