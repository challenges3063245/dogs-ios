//
//  ViewModel.swift
//  DogsApp
//
//  Created by Ariel Díaz on 08/11/23.
//

import Foundation
import Combine

protocol DogsViewModelProtocol {
    var output: DogsViewModelOutput { get }
    func bind(input: DogsViewModelInput) -> DogsViewModelOutput
}

class DogsViewModel: DogsViewModelProtocol {
    let output: DogsViewModelOutput
    private let useCase: DogsUseCase
    private var subscriptions = Set<AnyCancellable>()
    private var viewData: DogsViewData?

    init(useCase: DogsUseCase, output: DogsViewModelOutput = DogsViewModelOutput()) {
        self.useCase = useCase
        self.output = output 
    }

    // MARK: - binding
    /// -   bind function allows the viewModel to send and receive messages with the viewController through input/output 
    func bind(input: DogsViewModelInput) -> DogsViewModelOutput {
        input.didLoadPublisher.sink(receiveValue: didLoadHandler).store(in: &subscriptions)
        input.forceFetchDogsPublisher.sink(receiveValue: forceFetchDogsHandler).store(in: &subscriptions)
        input.didSelectRowPublisher.sink(receiveValue: didSelectRowHandler).store(in: &subscriptions)

        return self.output
    }

    private lazy var didLoadHandler: Completion = { [weak self] in
        guard let self = self else { return }
        self.useCase.getAvailableDogs().sink { [weak self] completion in
            if case .failure(let error) = completion {
                if DataSource.emptyError == error as NSError {
                    // case if CoreData returns empty array then fetch data from internet
                    self?.forceFetchDogsHandler()
                }
            }
        } receiveValue: { [weak self] dogs in
            self?.processDogData(dogs: dogs)
        }
        .store(in: &self.subscriptions)
    }

    private lazy var forceFetchDogsHandler: Completion = { [weak self] in
        guard let self = self else { return }
        self.useCase.fetchDogs().sink { [weak self] completion in
            if case .failure(let error) = completion {
                self?.output.updateUIErrorPublisher.send(error)
            }
        } receiveValue: { [weak self] dogs in
            self?.processDogData(dogs: dogs)
        }
        .store(in: &self.subscriptions)
    }

    private func processDogData(dogs: [Dog]) {
        let dogViewDataModels = dogs.map { DogViewData($0) }
        self.viewData = DogsViewData(dogViewDataModels)
        guard let viewData = self.viewData else { return }
        self.output.updateUIPublisher.send(viewData)
    }

    private lazy var didSelectRowHandler: (IndexPath) -> Void = { [weak self] indexPath in
        guard let viewData = self?.viewData?.dogViewModels[indexPath.row] else { return }
        self?.output.coordinateToDetailPublisher.send(viewData)
    }
}
