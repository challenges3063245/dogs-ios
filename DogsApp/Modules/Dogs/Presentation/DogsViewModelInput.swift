//
//  ViewModelInput.swift
//  DogsApp
//
//  Created by Ariel Díaz on 08/11/23.
//

import Combine
import Foundation

struct DogsViewModelInput {
    let didLoadPublisher = PassthroughSubject<Void, Never>()
    let forceFetchDogsPublisher = PassthroughSubject<Void, Never>()
    let didSelectRowPublisher = PassthroughSubject<IndexPath, Never>()
}
