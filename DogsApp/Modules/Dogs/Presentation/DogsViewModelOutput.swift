//
//  ViewModelOutput.swift
//  DogsApp
//
//  Created by Ariel Díaz on 08/11/23.
//

import Combine

struct DogsViewModelOutput {
    let updateUIPublisher = PassthroughSubject<DogsViewData, Never>()
    let updateUIErrorPublisher = PassthroughSubject<Error, Never>()
    let coordinateToDetailPublisher = PassthroughSubject<DogViewData, Never>()
}
